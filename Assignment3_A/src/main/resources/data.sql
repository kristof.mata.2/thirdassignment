--Franchise
INSERT INTO franchise (name,description)
VALUES ('Marvel Cinematic Univers',
        'The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios.'); --1
--Movie
INSERT INTO movie (title,genre, release_year,director,picture,trailer,franchise_id)
VALUES ('Iron Man',
        'action',
        2008,
        'Jon Favreau',
        'https://upload.wikimedia.org/wikipedia/en/0/02/Iron_Man_%282008_film%29_poster.jpg',
        'https://www.youtube.com/watch?v=8ugaeA-nMTc',
        1); --1
INSERT INTO movie (title,genre, release_year,director,picture,trailer,franchise_id)
VALUES ('Captain America: Civil War',
        'action',
        2016,
        'Anthony and Joe Russo',
        'https://upload.wikimedia.org/wikipedia/en/5/53/Captain_America_Civil_War_poster.jpg',
        'https://www.youtube.com/watch?v=FkTybqcX-Yo',
        1); --2
INSERT INTO movie (title,genre, release_year,director,picture,trailer,franchise_id)
VALUES ('Spider-Man: No Way Home',
        'action',
        2021,
        'Jon Watts',
        'https://upload.wikimedia.org/wikipedia/en/0/00/Spider-Man_No_Way_Home_poster.jpg',
        'https://www.youtube.com/watch?v=JfVOs4VSpmA',
        1); --3
--Character
INSERT INTO character(name,alias, gender,picture)
VALUES ('Tony Stark',
        'Iron Man',
        'male',
        'https://upload.wikimedia.org/wikipedia/en/f/f2/Robert_Downey_Jr._as_Tony_Stark_in_Avengers_Infinity_War.jpg');--1
INSERT INTO character(name,alias, gender,picture)
VALUES ('Steve Rogers',
        'Captain America',
        'male',
        'https://upload.wikimedia.org/wikipedia/en/6/6b/Chris_Evans_as_Steve_Rogers_Captain_America.jpg'); --2
INSERT INTO character(name,alias, gender,picture)
VALUES ('Peter Parker',
        'Spider-Man',
        'male',
        'https://upload.wikimedia.org/wikipedia/en/0/0f/Tom_Holland_as_Spider-Man.jpg');
--Characters in Movies
INSERT INTO character_movies (character_id, movie_id) VALUES (1,1);
INSERT INTO character_movies (character_id, movie_id) VALUES (1,2);
INSERT INTO character_movies (character_id, movie_id) VALUES (2,2);
INSERT INTO character_movies (character_id, movie_id) VALUES (3,3);
INSERT INTO character_movies (character_id, movie_id) VALUES (3,2);