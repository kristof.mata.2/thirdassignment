package com.example.assignment3_a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment3AApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment3AApplication.class, args);
    }

}
