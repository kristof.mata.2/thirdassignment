package com.example.assignment3_a.controllers;

import com.example.assignment3_a.mappers.CharacterMapper;
import com.example.assignment3_a.mappers.FranchiseMapper;
import com.example.assignment3_a.mappers.MovieMapper;
import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.DTOs.character.CharacterDTO;
import com.example.assignment3_a.models.DTOs.franchise.FranchiseDTO;
import com.example.assignment3_a.models.DTOs.franchise.FranchiseMoviesDTO;
import com.example.assignment3_a.models.Franchise;
import com.example.assignment3_a.models.Movie;
import com.example.assignment3_a.services.character.CharacterService;
import com.example.assignment3_a.services.franchise.FranchiseService;
import com.example.assignment3_a.services.movie.MovieService;
import com.example.assignment3_a.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    private final MovieService movieService;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieService movieService) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieService = movieService;
    }

    @Operation(summary = "Get all Franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "No Franchise exists",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping // GET: localhost:8080/api/v1/franchises
    public ResponseEntity getAll() {
        Collection<FranchiseDTO> franchise = franchiseMapper.franchiseToFranchiseDto(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(franchise);
    }


    @Operation(summary = "Get a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("{id}") // GET: localhost:8080/api/v1/franchises/1
    public ResponseEntity getById(@PathVariable int id) {
        FranchiseDTO franchise = franchiseMapper.franchiseToFranchiseDto(
                franchiseService.findById(id)
        );
        return ResponseEntity.ok(franchise);
    }

    @Operation(summary = "Search franchise by name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "No franchise exists",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("search") // GET: localhost:8080/api/v1/franchises/search?name=Marvel Cinematic Univers"
    public ResponseEntity<Collection<FranchiseDTO>> findByName(@RequestParam String name) {
        Collection<FranchiseDTO> chars = franchiseMapper.franchiseToFranchiseDto(
                franchiseService.findAllByName(name)
        );
        return ResponseEntity.ok(chars);
    }

    @Operation(summary = "Get a Movies of Franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("{id}/movies") // GET: localhost:8080/api/v1/franchises/1/movies
    public ResponseEntity getMoviesById(@PathVariable int id) {
        Collection<FranchiseMoviesDTO> movies = franchiseMapper.franchiseToFranchiseMoviesDTO(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(movies);
    }

    @Operation(summary = "Adds a new franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PostMapping // POST: localhost:8080/api/v1/franchises
    public ResponseEntity add(@RequestBody FranchiseDTO franchiseDTO) {

        Set<Integer> newSet = new HashSet<>();
        franchiseDTO.setMovies(newSet);
        Franchise newFranchise = franchiseMapper.franchiseDtoToFranchise(franchiseDTO);
        franchiseService.add(newFranchise);
        URI location = URI.create("franchise/" + newFranchise.getId());
        return ResponseEntity.created(location).build();
        // return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Updates a franchise.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") // PUT: localhost:8080/api/v1/franchises/1
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO, @PathVariable int id) {
        // Validates if body is correct
        if(id != franchiseDTO.getId())
            return ResponseEntity.badRequest().build();
        franchiseService.update(
                franchiseMapper.franchiseDtoToFranchise(franchiseDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "405",
                    description = "Method Not Allowed",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/franchises/1
    public ResponseEntity delete(@PathVariable int id) {
        Collection<Movie> movies = movieService.findAll();
        Franchise franchise = franchiseService.findById(id);
        for(Movie m: movies){
            if(m.getFranchise().getId()==franchise.getId()){
               m.setFranchise(null);
            }
        }
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }


}
