package com.example.assignment3_a.repositories;

import com.example.assignment3_a.models.Franchise;
import com.example.assignment3_a.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise,Integer> {

    @Query("select s from Franchise s where s.name like %?1%")
    Set<Franchise> findAllByName(String name);

}
