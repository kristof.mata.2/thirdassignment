package com.example.assignment3_a.repositories;

import com.example.assignment3_a.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CharacterRepository extends JpaRepository<Character,Integer> {
    @Query("select s from Character s where s.name like %?1%")
    Set<Character> findAllByName(String name);
}
