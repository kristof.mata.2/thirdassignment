package com.example.assignment3_a.repositories;

import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
    @Query("select s from Movie s where s.title like %?1%")
    Set<Movie> findAllByTitle(String title);
}
