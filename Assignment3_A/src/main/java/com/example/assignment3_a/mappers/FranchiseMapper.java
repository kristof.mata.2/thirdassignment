package com.example.assignment3_a.mappers;

import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.DTOs.character.CharacterDTO;
import com.example.assignment3_a.models.DTOs.franchise.FranchiseDTO;
import com.example.assignment3_a.models.DTOs.franchise.FranchiseMoviesDTO;
import com.example.assignment3_a.models.DTOs.movie.MovieDTO;
import com.example.assignment3_a.models.Franchise;
import com.example.assignment3_a.models.Movie;
import com.example.assignment3_a.services.character.CharacterService;
import com.example.assignment3_a.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises);

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }


    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovie")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO franchiseDTO);

    @Named("movieIdToMovie")
    Movie mapIdToMovie(int id) {
        return movieService.findById(id);
    }

    //@Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseMoviesDTO franchiseToFranchiseMoviesDTO(Franchise franchise);
    public abstract Collection<FranchiseMoviesDTO> franchiseToFranchiseMoviesDTO(Collection<Franchise> franchises);

}
