package com.example.assignment3_a.mappers;

import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.DTOs.character.CharacterDTO;
import com.example.assignment3_a.models.Franchise;
import com.example.assignment3_a.models.Movie;
import com.example.assignment3_a.services.character.CharacterService;
import com.example.assignment3_a.services.franchise.FranchiseService;
import com.example.assignment3_a.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {

    @Autowired
    protected MovieService movieService;
    @Autowired
    protected FranchiseService franchiseService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO characterToCharacterDto(Character characters);

    public abstract Collection<CharacterDTO> characterToCharacterDto(Collection<Character> characters);


    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovie")
    public abstract Character characterDtoToCharacter(CharacterDTO characterDTO);

    @Named("movieIdToMovie")
    Movie mapIdToMovie(int id) {
        return movieService.findById(id);
    }

}
