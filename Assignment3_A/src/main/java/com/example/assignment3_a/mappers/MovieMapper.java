package com.example.assignment3_a.mappers;

import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.DTOs.character.CharacterDTO;
import com.example.assignment3_a.models.DTOs.movie.MovieDTO;
import com.example.assignment3_a.models.Movie;
import com.example.assignment3_a.services.character.CharacterService;
import com.example.assignment3_a.services.franchise.FranchiseService;
import com.example.assignment3_a.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected CharacterService characterService;

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);


    @Named("charactersToIds")
    Set<Integer> mapCharactersToIds(Set<Character> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Mapping(target = "franchise.id", source = "franchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterToCharacterId")
    public abstract Movie movieIdtoToMovie(MovieDTO movieDTO);

    @Named("characterToCharacterId")
    Character mapCharacterToId(int id) {
        return characterService.findById(id);
    }

}
