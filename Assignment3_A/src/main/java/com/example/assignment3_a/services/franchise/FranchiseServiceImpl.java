package com.example.assignment3_a.services.franchise;

import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.Franchise;
import com.example.assignment3_a.models.Movie;
import com.example.assignment3_a.repositories.FranchiseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Iterator;
import java.util.HashSet;

@Service
public class FranchiseServiceImpl implements FranchiseService{
    private final Logger logger = LoggerFactory.getLogger(FranchiseServiceImpl.class);
    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository){
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Franchise findById(Integer integer) {
        return franchiseRepository.findById(integer).get();
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        franchiseRepository.deleteById(id);
    }

    @Override
    public void delete(Franchise entity) {
        if(franchiseRepository.existsById(entity.getId())){
            Franchise franchise = entity;
            Iterator<Movie> movieIterator = franchise.getMovies().iterator();
            while(movieIterator.hasNext()){                 //iterator is used, in order to avoid ConcurentModificationException
                Movie mov = movieIterator.next();
                movieIterator.remove();
            }
            franchiseRepository.delete(franchise);
        }else{
            logger.warn("No franchise exists with id:" + entity.getId());
        }
    }

    @Override
    public Collection<Franchise> findAllByName(String name) {

        return franchiseRepository.findAllByName(name);
    }

}
