package com.example.assignment3_a.services.character;

import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.Movie;
import com.example.assignment3_a.repositories.CharacterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Iterator;

@Service
public class CharacterServiceImpl implements CharacterService{

    private final Logger logger = LoggerFactory.getLogger(CharacterServiceImpl.class);
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository){
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer integer) {
        return characterRepository.findById(integer).get();
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        characterRepository.deleteById(id);
    }

    @Override
    public void delete(Character entity) {
        if(characterRepository.existsById(entity.getId())){
            Character character = entity;
            Iterator<Movie> movieIterator = character.getMovies().iterator();
            while(movieIterator.hasNext()){                 //iterator is used, in order to avoid ConcurentModificationException
                Movie mov = movieIterator.next();
                movieIterator.remove();
            }
            characterRepository.delete(character);
        }else{
            logger.warn("No character exists with ID: " + entity.getId());
        }
    }

    @Override
    public Collection<Character> findAllByName(String name) {

        return characterRepository.findAllByName(name);

    }
}
