package com.example.assignment3_a.services.character;


import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.services.CrudService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface CharacterService extends CrudService<Character,Integer> {
    //additional business logic
    Collection<Character> findAllByName(String name);
}
