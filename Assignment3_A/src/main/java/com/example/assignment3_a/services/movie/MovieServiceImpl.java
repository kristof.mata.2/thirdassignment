package com.example.assignment3_a.services.movie;

import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.Movie;
import com.example.assignment3_a.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Iterator;

@Service
public class MovieServiceImpl implements MovieService {

    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);
    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }
    @Override
    public Movie findById(Integer integer) {
        return movieRepository.findById(integer).get();
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        movieRepository.deleteById(id);
    }

    @Override
    public void delete(Movie entity) {
        if(movieRepository.existsById(entity.getId())) {
            Movie movie = entity;
            Iterator<Character> characterIterator = movie.getCharacters().iterator();
            while(characterIterator.hasNext()){                 //iterator is used, in order to avoid ConcurentModificationException
                Character character = characterIterator.next();
                characterIterator.remove();
            }
            movie.getFranchise().removeMovie(movie);
            movieRepository.delete(movie);
        }
        else {
            logger.warn("No movie exists with ID: " + entity.getId());
        }
    }

    @Override
    public Collection<Movie> findAllByTitle(String title) {

        return movieRepository.findAllByTitle(title);
    }


}
