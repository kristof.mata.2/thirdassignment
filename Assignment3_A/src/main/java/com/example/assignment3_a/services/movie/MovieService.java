package com.example.assignment3_a.services.movie;


import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.Movie;
import com.example.assignment3_a.services.CrudService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface MovieService extends CrudService<Movie,Integer> {
    //additional business logic
    Collection<Movie> findAllByTitle(String title);

}
