package com.example.assignment3_a.services.franchise;


import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.Franchise;
import com.example.assignment3_a.services.CrudService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface FranchiseService extends CrudService<Franchise,Integer> {
    //additional business logic
    Collection<Franchise> findAllByName(String name);
}
