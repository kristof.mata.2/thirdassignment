package com.example.assignment3_a.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

/**
 * Franchise entity model
 * Attributes:
 * ID - identification number
 * name - the name of the franchise
 * description - a short description of the franchise
 * movies - a collection of the movies from the database, that are part of the franchise
 */
@Getter
@Setter
@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50,nullable = false)
    private String name;
    @Column(length = 250,nullable = false)
    private String description;
    @OneToMany(mappedBy = "franchise",fetch = FetchType.EAGER)
    private Set<Movie> movies;

    public Set<Movie> getMovies() {
        return movies;
    }

    /**
     * Adds a movie to the movie collection
     * @param movie -the movie to add
     */
    public void  addMovie(Movie movie){
        this.movies.add(movie);
        movie.setFranchise(this);
    }
    /**
     * Removes a movie from the franchises movie collection
     * @param movie - the movie to remove
     */
    public void removeMovie(Movie movie){
        this.movies.remove(movie);
        movie.setFranchise(null);
    }
}
