package com.example.assignment3_a.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Character Entity Model
 * Attributes:
 * ID - identification number
 * name - the name of the character
 * alias - some other name or phrase they are known by
 * gender - their gender
 * picture - a link to a picture of the character
 * movies - a collection of movies they are in
 */


@Getter
@Setter
@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50,nullable = false)
    private String name;
    @Column(length = 50,nullable = false)
    private String alias;
    @Column(length = 50)
    private String gender;
    @Column
    private String picture; //URL to a picture of the character
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "character_movies",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Movie> movies;

    public int getId() {
        return id;
    }

    public String getAlias() {
        return alias;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getPicture() {
        return picture;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    /**
     * Adds a movie to the movie collection
     * @param movie -the movie to add
     */
    public void addMovie(Movie movie){
        this.movies.add(movie);
        movie.getCharacters().add(this);
    }

    /**
     * Removes a movie from the characters movie collection
     * @param movie - the movie to remove
     */
    public void removeMovie(Movie movie){
        this.movies.remove(movie);
        movie.getCharacters().remove(this);
    }

}
