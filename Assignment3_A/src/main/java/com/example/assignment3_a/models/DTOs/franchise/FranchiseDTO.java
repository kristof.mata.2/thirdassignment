package com.example.assignment3_a.models.DTOs.franchise;

import com.example.assignment3_a.models.Movie;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
public class FranchiseDTO {
    private int id;
    private String name;
    private String description;
    private Set<Integer> movies;
}
