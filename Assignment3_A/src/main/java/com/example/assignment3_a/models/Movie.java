package com.example.assignment3_a.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.boot.cloud.CloudPlatform;

import javax.persistence.*;
import java.util.Set;

/**
 * Movie Entity Model
 * Attributes:
 * ID - identification number
 * title - the title of the movie
 * genre - the genre of the movie
 * realeaseYear - the year the movie was realeased
 * director - the persons name who directed the movie
 * picture - a link to a picture of the movies poster
 * trailer - a link to the trailer video of the movie
 * chracters - a collection of characters from the database, who are in the movie
 * franchise - a object of the franchise, that the movie is part of
 */


@Getter
@Setter
@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100,nullable = false)
    private String title;
    @Column(length = 100,nullable = false)
    private String genre;
    @Column
    private int releaseYear;
    @Column(length = 50,nullable = false)
    private String director;
    @Column
    private String picture; //URL to a movie poster
    @Column
    private String trailer; //URL to a youtube trailer link
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToMany(mappedBy = "movies")
    private Set<Character> characters;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

    public Set<Character> getCharacters() {
        return characters;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    /**
     * Adds character to the character collection
     * @param character - the character to add
     */
    public void addCharacter(Character character) {
        this.characters.add(character);
        character.getMovies().add(this);
    }

    /**
     * Removes a Chracter from the character collection
     * @param character - the character to remove
     */
    public void removeCharacter(Character character) {
        this.characters.remove(character);
        character.getMovies().remove(this);
    }



}
