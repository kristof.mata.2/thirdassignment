package com.example.assignment3_a.models.DTOs.franchise;

import lombok.Data;

import java.util.Set;

@Data
public class FranchiseMoviesDTO {
    private Set<Integer> movies;
}
