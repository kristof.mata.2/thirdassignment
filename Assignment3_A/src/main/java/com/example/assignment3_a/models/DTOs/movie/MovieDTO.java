package com.example.assignment3_a.models.DTOs.movie;

import com.example.assignment3_a.models.Character;
import com.example.assignment3_a.models.Franchise;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String picture; //URL to a movie poster
    private String trailer; //URL to a youtube trailer link
    private Set<Integer> characters;
    private Integer franchise;
}
