## Name
Assigment 3

## Description
Create a PostgreSQL database using Hibernate and expose it through a deployed Web API. Follow the guidelines given
below, feel free to expand on the functionality. It must meet the minimum requirements prescribed.

## Application
![image.png](./image.png)
![image-1.png](./image-1.png)
![image-2.png](./image-2.png)
## Installation
Install JDK 17+
Install PostgreSQL - pgAdmin 4
Create a database in pgAdmin named moviedb
Install Intellij
Clone repository
Modify application.properties with your pgAdmin 4 password.
Make sure in application.properties the database url points to a valid database.

## Usage
Run the application
Access various endpoint in the browser.

## Maintainers
@tothbt
@kristof.mata.2

## Project status
Development has slowed down or stopped completely.
